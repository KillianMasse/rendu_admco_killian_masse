#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
author : killian masse

"""

import unittest
import logging

from Calculator.simplecalculator import SimpleCalculator


class AdditionTest(unittest.TestCase):
    """
    Classe addition gérée avec unittest
    """

    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleCalculator()

    def test_somme_deux_entiers(self):
        """somme de deux entiers"""
        result = self.calculator.somme(30, 5)
        self.assertEqual(result, 35)
        logging.warning("test_addition ok warn")
        logging.info("test_addition ok info")

    def test_addition_entier_string(self):
        """somme entier et string"""
        result = self.calculator.somme(30, "5")
        self.assertEqual(result, "ERROR")
        logging.warning("test_addition ok warn")
        logging.info("test_addition ok info")

    def test_addition_entiers_negatifs(self):
        """somme entiers negatifs"""
        result = self.calculator.somme(-30, -5)
        self.assertEqual(result, -35)
        self.assertNotEqual(result, 35)
        logging.warning("test_addition ok warn")
        logging.info("test_addition ok info")


class SoustractionTest(unittest.TestCase):
    """
    Classe soustraction gérée avec unittest
    """

    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleCalculator()

    def test_soustraction_deux_entiers(self):
        """soustraction de deux entiers"""
        result = self.calculator.soustraction(30, 5)
        self.assertEqual(result, 25)
        logging.warning("test_soustraction ok warn")
        logging.info("test_soustraction ok info")

    def test_soustraction_entier_string(self):
        """soustraction entier et string"""
        result = self.calculator.soustraction(30, "5")
        self.assertEqual(result, "ERROR")
        logging.warning("test_soustraction ok warn")
        logging.info("test_soustraction ok info")

    def test_soustraction_entiers_negatifs(self):
        """soustraction de deux entiers negatifs"""
        result = self.calculator.soustraction(-30, -5)
        self.assertEqual(result, -25)
        self.assertNotEqual(result, -35)
        logging.warning("test_soustraction ok warn")
        logging.info("test_soustraction ok info")


class MultiplicationTest(unittest.TestCase):
    """
    Classe multiplication gérée avec unittest
    """

    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleCalculator()

    def test_multiplication_deux_entiers(self):
        """multiplication de deux entiers"""
        result = self.calculator.multiplication(30, 5)
        self.assertEqual(result, 150)
        logging.warning("test_multiplication ok warn")
        logging.info("test_multiplication ok info")

    def test_multiplication_entier_string(self):
        """multiplication entier et string"""
        result = self.calculator.multiplication(30, "5")
        self.assertEqual(result, "ERROR")
        logging.warning("test_multiplication ok warn")
        logging.info("test_multiplication ok info")

    def test_multiplication_entiers_negatifs(self):
        """multiplication entiers negatifs"""
        result = self.calculator.multiplication(-30, -5)
        self.assertEqual(result, 150)
        self.assertNotEqual(result, -150)
        logging.warning("test_multiplication ok warn")
        logging.info("test_multiplication ok info")


class DivisionTest(unittest.TestCase):
    """
    Classe division gérée avec unittest
    """

    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleCalculator()

    def test_division_deux_entiers(self):
        """division de deux entiers"""
        result = self.calculator.division(30, 5)
        self.assertEqual(result, 6)
        logging.warning("test_division ok warn")
        logging.info("test_division ok info")

    def test_division_entier_string(self):
        """division entier et string"""
        result = self.calculator.division(30, "5")
        self.assertEqual(result, "ERROR")
        logging.warning("test_division ok warn")
        logging.info("test_division ok info")

    def test_division_entiers_negatifs(self):
        """division deux entiers negatifs"""
        result = self.calculator.division(-30, -5)
        self.assertEqual(result, 6)
        self.assertNotEqual(result, -6)
        logging.warning("test_division ok warn")
        logging.info("test_division ok info")

    def test_divide_by_zero_exception(self):
        """division par zero"""
        with self.assertRaises(ZeroDivisionError):
            self.calculator.division(10, 0)



if __name__ == "__main__":
    unittest.main()
