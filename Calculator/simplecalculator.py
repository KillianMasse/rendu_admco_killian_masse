# -*- coding: utf-8 -*-
"""
Created on Mon Mar 23 12:43:34 2020
@author: Killian Massé

Ajout de test sur les types
"""


class SimpleCalculator:
    """
    Operation with two variables

    Param : arg1 (int): first variable of the operation
	    arg2 (int): second variable of the operation

    Return : Result of the operation
    """

    def __init__(self):
        """ """

    def somme(self, nombre1, nombre2):
        """
        fonction somme
        """
        if isinstance(nombre1, int) and isinstance(nombre2, int):
            return nombre1 + nombre2
        return "ERROR"

    def soustraction(self, nombre1, nombre2):
        """
        fonction soustraction
        """
        if isinstance(nombre1, int) and isinstance(nombre2, int):
            return nombre1 - nombre2
        return "ERROR"

    def multiplication(self, nombre1, nombre2):
        """
        fonction multiplication
        """
        if isinstance(nombre1, int) and isinstance(nombre2, int):
            return nombre1 * nombre2
        return "ERROR"

    def division(self, nombre1, nombre2):
        """
        fonction division
        """
        if isinstance(nombre1, int) and isinstance(nombre2, int):
            if nombre2 == 0:
                raise ZeroDivisionError("Connot divide by zero")
            return nombre1 / nombre2
        return "ERROR"
