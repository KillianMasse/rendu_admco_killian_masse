# RENDU TP ADMCO

## Objectif
Nous avions pour objectif de réaliser une **intégration continue** afin de vérifier le bon fonctionnement du projet

## Travail effectué
J'ai ajouté un fichier **gitlab-ci.yml** afin de réaliser l'intégration continue
Réalisation d'un fichier **setup.py** pour l'archivage
J'ai modifié les fichier **simplecalculator.py** et **my_test.py** afin qu'il correspondent à cette intégration continue

## Travail notable
J'ai réalisé différents test pour chaque opération et j'ai étudié la division par zero plus en détail
J'ai modifié et ajouté des informations au **setup.py** afin qu'il corresponde à mon travail et qu'il soit facilement accessible
La réalisation du **README.md** a pris du temps afin de pouvoir fournir des informations de qualité

## Notation
Les différents codes ont été vérifiés à l'aide de **pylint** et **black** afin d'avoir la meilleure notation possible. De plus, le code **.gitlab-ci.yml** a permis de vérifié sous **pycharm** le bon fonctionnement de **my_test.py** (tests unitaires)

## Test pipelines
Dans gitlab, on peut voir que les différentes étapes pipeline sont bien effectuées : Static Analysis, Test et Lib Generation (coché en vert):

  ![pipelines](captures_readme/pipelines.png)

## Dossier
Le TP est disponible à cet adresse :
https://gitlab.com/KillianMasse/rendu_admco_killian_masse


